package spb.ubooks.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import spb.ubooks.entity.CommentEntity;

public interface CommentRepository extends CrudRepository<CommentEntity, Integer>{
	List<CommentEntity> findByBookId(int bookId);
	List<CommentEntity> findByBookIdAndDeleteYn(int bookId, char deleteYn);
}
