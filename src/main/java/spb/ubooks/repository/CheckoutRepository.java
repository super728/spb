package spb.ubooks.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import spb.ubooks.entity.CheckoutEntity;

public interface CheckoutRepository extends CrudRepository<CheckoutEntity,String>{
	List<CheckoutEntity> findByFullName(String fullName);
}
