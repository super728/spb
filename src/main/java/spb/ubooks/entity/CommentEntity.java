package spb.ubooks.entity;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name="spb_comment")
@NoArgsConstructor
@Data
@ApiModel(value="CommentEntity : 상품 리뷰 댓글목록", description="상품 리뷰 댓글목록")
public class CommentEntity {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@ApiModelProperty(value="리뷰 번호")
	private int idx;
	
	
	@Column(nullable=false)
	@ApiModelProperty(value="작성자 아이디")
	private String userId;
	
	@Column(nullable=false)
	@ApiModelProperty(value="작성자 이름")
	private String userName;
	
	@Column(nullable=false)
	@ApiModelProperty(value="상품 번호")
	private int bookId;
	
	@Column(nullable=false)
	@ApiModelProperty(value="리뷰 내용")
	private String content;
	
	@Column(nullable=true)
	@ApiModelProperty(value="리뷰 삭제 여부")
	private char deleteYn='n';
	
	@Column(nullable=false)
	@ApiModelProperty(value="리뷰 작성 시간")
	private LocalDateTime timestamp = LocalDateTime.now();
	
	@Column(nullable=true)
	@ApiModelProperty(value="리뷰 수정 시간")
	private LocalDateTime updated_date;
	
	@Column(nullable=true)
	@ApiModelProperty(value="리뷰 삭제 시간")
	private LocalDateTime deleted_date;
}
