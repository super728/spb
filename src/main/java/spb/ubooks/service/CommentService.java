package spb.ubooks.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import spb.ubooks.entity.CommentEntity;

public interface CommentService {
	String addComment(HttpServletRequest request, CommentEntity commentEntity) throws Exception;
	List<CommentEntity> getComment(int bookId) throws Exception;
	String updateComment(HttpServletRequest request, CommentEntity commentEntity) throws Exception;
	String deleteComment(HttpServletRequest request, int idx) throws Exception;
}
