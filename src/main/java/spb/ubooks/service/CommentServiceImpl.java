package spb.ubooks.service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;
import spb.ubooks.entity.CommentEntity;
import spb.ubooks.repository.CheckoutRepository;
import spb.ubooks.repository.CommentRepository;

@Slf4j
@Service
public class CommentServiceImpl implements CommentService{
	
	@Autowired
	CheckoutRepository checkoutRepository;
	
	@Autowired
	CommentRepository commentRepository;

	@Override
	public String addComment(HttpServletRequest request, CommentEntity commentEntity) throws Exception {
		HttpSession session = request.getSession();
		if(session.getAttribute("memberId") != null) {
			String memberId = session.getAttribute("memberId").toString().trim();
			String memberName = session.getAttribute("memberName").toString().trim();
			// 1. memberId로 checkout table 조회하여 구매 내역 확인
			int size = checkoutRepository.findByFullName(memberId).size();
			if(size > 0) { 
				// 구매한 유저
				// 2. commentRepository 로 commentEntity 저장
				commentEntity.setUserId(memberId);
				commentEntity.setUserName(memberName);
				commentRepository.save(commentEntity);
				return "redirect:/complete-works/" + commentEntity.getBookId();
			}
		}
		return "redirect:/invalidComment";
		
	}

	@Override
	public List<CommentEntity> getComment(int bookId) throws Exception {
		return commentRepository.findByBookIdAndDeleteYn(bookId,'n');
	}

	@Override
	public String updateComment(HttpServletRequest request, CommentEntity commentEntity) throws Exception {
		HttpSession session = request.getSession();
		if(session.getAttribute("memberId") != null) {
			String memberId = session.getAttribute("memberId").toString().trim();
			int idx = commentEntity.getIdx();
			
			Optional<CommentEntity> optional = commentRepository.findById(idx);
			
			if(optional.isPresent()) {
				CommentEntity comment = optional.get();
				if(memberId.equals(comment.getUserId())) {
					comment.setContent(commentEntity.getContent());
					comment.setUpdated_date(LocalDateTime.now());
					CommentEntity result  = commentRepository.save(comment);
					return "/complete-works/" + comment.getBookId();
				}
			} else {
				throw new NullPointerException();
			}
		}
		return "/invalidCommentUpdate";
	}

	@Override
	public String deleteComment(HttpServletRequest request, int idx) throws Exception {
		HttpSession session = request.getSession();
		if(session.getAttribute("memberId") != null) {
			String memberId = session.getAttribute("memberId").toString().trim();
			
			Optional<CommentEntity> optional = commentRepository.findById(idx);
			
			if(optional.isPresent()) {
				CommentEntity comment = optional.get();
				if(memberId.equals(comment.getUserId())) {
					comment.setDeleted_date(LocalDateTime.now());
					comment.setDeleteYn('y');
					commentRepository.save(comment);
					return "/complete-works/" + comment.getBookId();
				}
			} else {
				throw new NullPointerException();
			}
		}
		return "/invalidCommentDelete";
	}

}
